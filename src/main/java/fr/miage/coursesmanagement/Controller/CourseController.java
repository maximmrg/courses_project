package fr.miage.coursesmanagement.Controller;

import fr.miage.coursesmanagement.Entity.Course;
import fr.miage.coursesmanagement.Entity.Student;
import fr.miage.coursesmanagement.Entity.User;
import fr.miage.coursesmanagement.Repository.CourseRepository;
import fr.miage.coursesmanagement.Repository.UserRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/courses")
public class CourseController {

    private final UserRepository ur;
    private final CourseRepository cr;

    @GetMapping
    public ResponseEntity<?> getAllCourses(){
        Iterable<Course> allCourses = cr.findAll();
        return ResponseEntity.ok(allCourses);
    }

    @PostMapping("/{courseId}/{userId}")
    public ResponseEntity<?> affectUser(@PathVariable("courseId") Long courseId, @PathVariable("userId") Long userId){
        Course course = cr.getById(courseId);

        User user = ur.getById(userId);

        course.getListUsers().add(user);

        cr.save(course);

        return ResponseEntity.ok().build();
    }

}
