package fr.miage.coursesmanagement.Controller;

import fr.miage.coursesmanagement.Entity.User;
import fr.miage.coursesmanagement.Repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserRepository ur;

    @GetMapping
    public ResponseEntity<?> getAllUsers(){
        Iterable<User> allUsers = ur.findAll();
        return ResponseEntity.ok(allUsers);
    }
}
