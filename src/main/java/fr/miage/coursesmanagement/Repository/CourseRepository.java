package fr.miage.coursesmanagement.Repository;

import fr.miage.coursesmanagement.Entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}
