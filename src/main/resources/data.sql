INSERT INTO User (id, lastname, firstname) VALUES
(1, 'Gobillard', 'Tom'),
(2, 'Wow', 'Tom'),
(3, 'Hey', 'Tom'),
(4, 'LeCrack', 'Tom');

INSERT INTO Classroom (id, number) VALUES
(1, 52);

INSERT INTO Course (id, name, formation, classroom_id) VALUES
(1, 'maths', 'MIAGE', 1);